#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "commandLine/commandLineParser.h"
#include "msiKeyboard/msiKeyboardCommunicator.h"

int main(int argc, char* argv[])
{
    std::list<CommandLineParser::ParsedCommand>* commands = nullptr;

    try {
        commands = CommandLineParser::parseCommandLineArgs(argc, argv);

        MsiKeyboardCommunicator::init();

        auto it = commands->begin();
        while (it != commands->end()) {
            MsiKeyboardCommunicator::sendCommandToKeyboard(*it);
            it++;
        }

        MsiKeyboardCommunicator::close();
    } catch (std::invalid_argument &e) {
        if(commands != nullptr) {
            delete commands;
        }

        printf(e.what());
    }

    return 0;
}