
#ifndef MSK_MSIKEYBOARDCOMMUNICATION_H
#define MSK_MSIKEYBOARDCOMMUNICATION_H

#include "../commandLine/commandLineParser.h"
#include "msiKeyboardFeatureReportHeader.h"
#include <hidapi/hidapi.h>

namespace MsiKeyboardCommunicator {
    enum IDs {
        VENDOR_ID = 0x1770,
        PRODUCT_ID = 0xFF00
    };

    static MsiKeyboardFeatureReportHeader::Header header;
    static hid_device *handle;

    static void init() {
        if (hid_init() != 0) {
            throw std::invalid_argument("Falied to init hidapi");
        }

        handle = hid_open(VENDOR_ID, PRODUCT_ID, nullptr);

        if (handle == nullptr) {
            hid_exit();
            throw std::invalid_argument("Falied to init keyboard, missing permissions maybe?");
        }
    }

    static void sendCommandToKeyboard(const CommandLineParser::ParsedCommand &command) {
        header.op = command.op;
        header.args1 = command.args1;
        header.args2 = command.args2;
        header.args3 = command.args3;
        header.args4 = command.args4;

        hid_send_feature_report(handle, reinterpret_cast<const unsigned char *>(&header), 8);
    }

    static void close() {
        hid_close(handle);

        if (hid_exit() != 0) {
            throw std::invalid_argument("Falied to close hidapi");
        }
    }
};

#endif
