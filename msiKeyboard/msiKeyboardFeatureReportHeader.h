
#ifndef MSIKEYBOARDFEATUREREPORTHEADER_H
#define MSIKEYBOARDFEATUREREPORTHEADER_H

namespace MsiKeyboardFeatureReportHeader {

    enum DefaultValues {
        HEADER1 = 1,
        HEADER2 = 2,
        FOOTER1 = 236,
        BLANK = 0
    };

    struct Header {
        unsigned char header1 = DefaultValues::HEADER1;
        unsigned char header2 = DefaultValues::HEADER2;
        unsigned char op = DefaultValues::BLANK;
        unsigned char args1 = DefaultValues::BLANK;
        unsigned char args2 = DefaultValues::BLANK;
        unsigned char args3 = DefaultValues::BLANK;
        unsigned char args4 = DefaultValues::BLANK;
        unsigned char footer1 = DefaultValues::FOOTER1;
    };
}

#endif
