
#ifndef MSIKEYBOARDCODES_H
#define MSIKEYBOARDCODES_H

namespace MsiKeyboardCodes {

    enum Op {
        OP_CODE_SET_COLOR = 64,
        OP_CODE_SET_MODE = 65,
    };

    enum Region {
        REGION_CODE_LEFT = 1,
        REGION_CODE_MIDDLE = 2,
        REGION_CODE_RIGHT = 3,
        REGION_CODE_LOGO = 4,
        REGION_CODE_FRONT_LEFT = 5,
        REGION_CODE_FRONT_RIGHT = 6,
        REGION_CODE_TOUCHPAD = 7
    };

    enum Mode {
        MODE_CODE_NORMAL = 1,
        MODE_CODE_GAMING = 2,
        MODE_CODE_BREATHE = 3,
        MODE_CODE_DEMO = 4,
        MODE_CODE_WAVE = 5
    };
}

#endif
