
#ifndef UTILS_H
#define UTILS_H

#include <cctype>

namespace Utils {
    static bool cStrIsNumber(const char* cStr) {
        for(int i = 0; cStr[i] != '\0'; i++) {
            if(!std::isdigit(cStr[i])) {
                return false;
            }
        }
        return true;
    }

    static bool convertCStrToByte(const char* cStr, unsigned char& result) {
        if(!cStrIsNumber(cStr)) {
            return false;
        }

        int tempResult = std::stoi(cStr);

        if (tempResult > 255) {
            return false;
        }

        result = static_cast<unsigned char>(tempResult);

        return true;
    }
}

#endif
