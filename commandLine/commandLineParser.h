
#ifndef COMMANDLINEPARSER_H
#define COMMANDLINEPARSER_H

#include <list>
#include <string>
#include <map>

#include "../msiKeyboard/msiKeyboardCodes.h"
#include "../msiKeyboard/msiKeyboardFeatureReportHeader.h"
#include "../utils/utils.h"

namespace CommandLineParser {

    const static std::map<std::string, unsigned char> operatorsTextBinds {
            {"--color", MsiKeyboardCodes::OP_CODE_SET_COLOR},
            {"--mode", MsiKeyboardCodes::OP_CODE_SET_MODE}};

    const static std::map<std::string, unsigned char> regionsTextBinds {
            {"left", MsiKeyboardCodes::REGION_CODE_LEFT},
            {"middle", MsiKeyboardCodes::REGION_CODE_MIDDLE},
            {"right", MsiKeyboardCodes::REGION_CODE_RIGHT},
            {"logo", MsiKeyboardCodes::REGION_CODE_LOGO},
            {"front_left", MsiKeyboardCodes::REGION_CODE_FRONT_LEFT},
            {"front_right", MsiKeyboardCodes::REGION_CODE_FRONT_RIGHT},
            {"touchpad", MsiKeyboardCodes::REGION_CODE_TOUCHPAD}};

    const static std::map<std::string, unsigned char> modeTextBinds {
            {"normal", MsiKeyboardCodes::MODE_CODE_NORMAL},
            {"gaming", MsiKeyboardCodes::MODE_CODE_GAMING},
            {"breathe", MsiKeyboardCodes::MODE_CODE_BREATHE},
            {"demo", MsiKeyboardCodes::MODE_CODE_DEMO},
            {"wave", MsiKeyboardCodes::MODE_CODE_WAVE}};

    struct ParsedCommand {
        unsigned char op = MsiKeyboardFeatureReportHeader::BLANK;
        unsigned char args1 = MsiKeyboardFeatureReportHeader::BLANK;
        unsigned char args2 = MsiKeyboardFeatureReportHeader::BLANK;
        unsigned char args3 = MsiKeyboardFeatureReportHeader::BLANK;
        unsigned char args4 = MsiKeyboardFeatureReportHeader::BLANK;
    };

    enum ArgsCountForOpsPlus1 {
        ARGS_COUNT_COLOR = 5,
        ARGS_COUNT_MODE = 2
    };

    std::list<ParsedCommand>* parseCommandLineArgs(int argc, char *argv[]) {
        const auto result = new std::list<ParsedCommand>;

        //Place op index to 1, 0 being the name of the executable
        unsigned int opPosition = 1;

        //Loop while the op index is not pointing on the last element
        while (opPosition != argc) {
            ParsedCommand command;

            {
                const std::string op = argv[opPosition];
                auto it = operatorsTextBinds.find(op);

                if (it == operatorsTextBinds.end()) {
                    throw std::invalid_argument("Op " + op + " does not exist");
                }
                command.op = it->second;
            }

            switch (command.op) {
                case MsiKeyboardCodes::OP_CODE_SET_COLOR: {
                    if(opPosition + ARGS_COUNT_COLOR > argc) {
                        throw std::invalid_argument("Not enough args passed for --color");
                    }

                    std::string region = argv[opPosition + 1];
                    auto it = regionsTextBinds.find(region);
                    if(it == regionsTextBinds.end()) {
                        throw std::invalid_argument("Region " + region + " does not exist");
                    }
                    command.args1 = it->second;

                    if(!Utils::convertCStrToByte(argv[opPosition + 2], command.args2)) {
                        throw std::invalid_argument("Invalid number (0-255) in args " + std::string(argv[opPosition + 2]));
                    }
                    if(!Utils::convertCStrToByte(argv[opPosition + 3], command.args3)) {
                        throw std::invalid_argument("Invalid number (0-255) in args " + std::string(argv[opPosition + 3]));
                    }
                    if(!Utils::convertCStrToByte(argv[opPosition + 4], command.args4)) {
                        throw std::invalid_argument("Invalid number (0-255) in args " + std::string(argv[opPosition + 4]));
                    }

                    opPosition += ARGS_COUNT_COLOR;
                }
                    break;
                case MsiKeyboardCodes::OP_CODE_SET_MODE: {
                    if(opPosition + ARGS_COUNT_MODE > argc) {
                        throw std::invalid_argument("Not enough args passed for --mode");
                    }

                    std::string mode = argv[opPosition + 1];
                    auto it = modeTextBinds.find(mode);
                    if(it == modeTextBinds.end()) {
                        throw std::invalid_argument("Mode " + mode + " does not exist");
                    }
                    command.args1 = it->second;

                    opPosition += ARGS_COUNT_MODE;
                }
                    break;
            }

            result->push_back(command);
        }

        return result;
    }
}

#endif